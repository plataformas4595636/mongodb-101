# Mongodb 101
Instrucciones para usar el programa:

#1) Descarga el archivo grades.json y ejecuta el siguiente comando en la terminal para importar los datos a la base de datos de MongoDB:

$ mongoimport -d students -c grades < grades.json

#2) Para confirmar que la colección se importó correctamente, abre la terminal de MongoDB y ejecuta los siguientes comandos:

    >use students; 
    >db.grades.count() 

    ¿Cuántos registros arrojó el comando count? 
    Respuesta: 800

#3) Para encontrar todas las calificaciones del estudiante con el ID número 4, utiliza el siguiente comando en la terminal de MongoDB:

    db.grades.find({ student_id: 4 });

#4) ¿Cuántos registros hay de tipo exam? 
   Respuesta: Utiliza el siguiente comando en la terminal de MongoDB: 

    db.grades.find({ type: 'exam' }).count();
    Respuesta: 200

#5) ¿Cuántos registros hay de tipo homework? 
    Respuesta: Utiliza el siguiente comando en la terminal de MongoDB:

    db.grades.find({ type: 'homework' }).count();
    Respuesta: 400

#6) ¿Cuántos registros hay de tipo quiz? 
    Respuesta: Utiliza el siguiente comando en la terminal de MongoDB:

    db.grades.find({ type: 'quiz' }).count();
    Respuesta: 200

#7) Para eliminar todas las calificaciones del estudiante con el ID número 3, utiliza el siguiente comando en la terminal de MongoDB:

    db.grades.deleteMany({ student_id: 3 });

#8) ¿Qué estudiantes obtuvieron 75.29561445722392 en una tarea? 
   Utiliza el siguiente comando en la terminal de MongoDB:

    db.grades.find({ score: 75.29561445722392, type: 'homework' });

#9) Para actualizar las calificaciones del registro con el ID "50906d7fa3c412bb040eb591" por 100, utiliza el siguiente comando en la terminal de MongoDB:

    db.grades.updateOne({ _id: ObjectId('50906d7fa3c412bb040eb591') }, { $set: { score: 100 } });

#10) Para encontrar a qué estudiante pertenece la calificación con el ID "50906d7fa3c412bb040eb591", utiliza el siguiente comando en la terminal de MongoDB:

    db.grades.find({ _id: ObjectId('50906d7fa3c412bb040eb591') });



## Authors and acknowledgment
Ricardo Carrete - 307746 
Jair Delval Aguirre contribuyò ayudandome a realizar la tarea

## License
For open source projects, say how it is licensed.


